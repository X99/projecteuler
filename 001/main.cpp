#include <iostream>

auto main() -> int
{
    int sum{ 0 };
    int upper_bound{ 1000 };

    for (int i{ 0 }; i < upper_bound; i++) {
        if (!(i % 3) || !(i % 5)) sum += i;
    }

    std::cout << sum << std::endl;

    return sum;
}