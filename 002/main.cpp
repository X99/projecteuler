#include <iostream>

auto main() -> int
{
    uint64_t sum{ 0 };
    uint64_t value{ 1 };
    uint64_t previous_value{ 1 };
    uint64_t tmp{ 0 };


    while (value < 4000000) {
        // compute next value
        tmp = value;
        value += previous_value;
        previous_value = tmp;

        if (!(value % 2)) sum += value;
    }

    std::cout << sum << " ";

    return sum;
}